package com.felixsoares.pocklayout

import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.support.constraint.ConstraintSet
import android.support.v4.widget.NestedScrollView
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import com.transitionseverywhere.ChangeBounds
import com.transitionseverywhere.Rotate
import com.transitionseverywhere.TransitionManager
import kotlinx.android.synthetic.main.layout_referencia_2.*


class MainActivity : AppCompatActivity() {

    private var collapsed = true
    private var headerCollapse = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_referencia_2)

        //setSupportActionBar(toolbar)
        //supportActionBar?.title = "Pock Toolbar"

        txtMenu.setOnClickListener {
            collapsed = !collapsed

            TransitionManager.beginDelayedTransition(llRoot)
            cardView.visibility = if (collapsed) View.VISIBLE else View.GONE
            nestedscroll.visibility = if (collapsed) View.VISIBLE else View.GONE

            TransitionManager.beginDelayedTransition(llRoot, ChangeBounds())

            if (!headerCollapse) {
                txtValue.setTextSize(TypedValue.COMPLEX_UNIT_SP, if (collapsed) 32.toFloat() else 22.toFloat())
            }

            val params = constraint.layoutParams
            params.height = if (collapsed) ViewGroup.LayoutParams.WRAP_CONTENT else ViewGroup.LayoutParams.MATCH_PARENT

            constraint.layoutParams = params

            constraint.setPadding(0, 15, 0, if (collapsed) convertDpToPixel(40) else 15)

            val set = ConstraintSet()
            set.clone(constraint)

            if (collapsed) {
                set.clear(llContent.id, ConstraintSet.TOP)
                set.clear(llContent.id, ConstraintSet.BOTTOM)

                set.clear(txtMenu.id, ConstraintSet.BOTTOM)
                set.connect(txtMenu.id, ConstraintSet.TOP, txtValue.id, ConstraintSet.BOTTOM, 0)
            } else {
                set.connect(txtMenu.id, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0)
                set.clear(txtMenu.id, ConstraintSet.TOP)

                set.connect(llContent.id, ConstraintSet.TOP, txtValue.id, ConstraintSet.BOTTOM, 0)
                set.connect(llContent.id, ConstraintSet.BOTTOM, txtMenu.id, ConstraintSet.TOP, 0)
            }

            set.applyTo(constraint)

            txtName.visibility = if (collapsed) if (headerCollapse) View.GONE else View.VISIBLE else View.VISIBLE
            llContent.visibility = if (collapsed) View.GONE else View.VISIBLE

            Handler().postDelayed({
                TransitionManager.beginDelayedTransition(constraint, Rotate())
                txtMenu.rotation = if (collapsed) 0f else 180f
            }, 350)
        }

        nestedscroll.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { _, _, scrollY, _, oldScrollY ->
            Handler().postDelayed({
                if (scrollY == 0 && headerCollapse) {
                    // "TOP SCROLL"
                    headerCollapse = false
                    toggleWhenScroll(!headerCollapse)
                } else if (!headerCollapse) {
                    // "Scroll DOWN"
                    headerCollapse = true
                    toggleWhenScroll(!headerCollapse)
                }
            }, 200)
        })
    }

    private fun toggleWhenScroll(collapse: Boolean) {
        Handler().postDelayed({
            val transition = ChangeBounds()
            transition.duration = 600

            TransitionManager.beginDelayedTransition(llRoot, transition)
            val params = cardView.layoutParams
            params.height = convertDpToPixel(if (collapse) 100 else 70)
            cardView.layoutParams = params

            txtValue.setTextSize(TypedValue.COMPLEX_UNIT_SP, if (collapse) 32.toFloat() else 22.toFloat())
            txtName.visibility = if (collapse) View.VISIBLE else View.GONE
        }, 100)
    }

    private fun convertDpToPixel(dp: Int): Int {
        val metrics = Resources.getSystem().displayMetrics
        val px = dp * (metrics.densityDpi / 160f)
        return Math.round(px)
    }
}
